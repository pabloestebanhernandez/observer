package observer.pattern.demo;


public class Observer implements IObserver{

	@Override
	public void update(int i){
		System.out.println("\tObserver: My value in subject is now "+ i);

	}

}
