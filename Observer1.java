package observer.pattern.demo;

public class Observer1 implements IObserver{


	@Override
	public void update(int i){

		System.out.println("\tObserver1: observer -> my value is change in subject to "+ i);


	}
}
