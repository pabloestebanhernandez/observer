package observer.pattern.demo;


class ObserverPatternEx{
	

	public static void main (String[] args){


		System.out.println("***Observer pattern demo");
		Observer o = new Observer();
		Observer1 o1 = new Observer1();

		Subject sub1 = new Subject();

		sub1.register(o);
		sub1.register(o1);

		System.out.println("Setting flag = 5");
		sub1.setFlag(5);

		System.out.println("Setting flag = 25");
		sub1.setFlag(25);

		//unregister
		sub1.unregister(o1);

		System.out.println("Setting flag = 50");
		sub1.setFlag(50);

	}

}